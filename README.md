Pioneer_DDJWeGO
===============

This mapping for the Pioneer DDJ-WeGO was made by Samuel Nicholas (nicholas.samuel@gmail.com)
derived from the works of:
* Joan Ardiaca Jové (joan.ardiaca@gmail.com, https://github.com/jardiacaj/mixxx) for the Pioneer DDJ-SB
* wingcom (wwingcomm@gmail.com, https://github.com/wingcom/Mixxx-Pioneer-DDJ-SB).
* Hilton Rudham (https://github.com/hrudham/Mixxx-Pioneer-DDJ-SR).
    
    this mapping continues the tradition of being pusblished under the MIT license.